<?php 
	require 'database.php';
	$id = null;
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( null==$id ) {
		header("Location: index.php");
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM agent where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
    
    			<div class="span10 offset1">
    				<div class="row">
		    			<h3>Other addresses of Agent</h3>
		    		</div>
		    		
	    			<div class="form-horizontal" >
					  <div class="control-group">
					    <label class="control-label">Agent Name</label>
					    <div class="controls">
						    <label class="checkbox">
						     	<?php echo $data['name'];?>
						    </label>
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label">Main Address</label>
					    <div class="controls">
					      	<label class="checkbox">
						     	<?php echo $data['mainaddress'];?>
						    </label>
					    </div>
					  </div>
                      <div class="control-group">
                      	<label class="control-label">Other Addresses</label>
                        <div class="controls">
                        <a href="create_address.php?id=<?php echo $id ?>" class="btn btn-success">Add address</a>
                        <p>
                        </div>
                        <div class="controls">
						<?php
                           $pdo = Database::connect();
                           $sql = 'SELECT * FROM address WHERE agent_id ='.$id;
                           foreach ($pdo->query($sql) as $row) {
							   		echo '<div class="control-group">';
                                    echo $row['address'];
									echo '<div class="controls">';
                                    echo '<a class="btn btn-success" href="update_address.php?id='.$row['id'].'">Update</a>';
                                    echo '&nbsp;';
                                    echo '<a class="btn btn-danger" href="delete_address.php?id='.$row['id'].'">Delete</a>';
									echo '</div>';
									echo '</div>';
                           }
                           Database::disconnect();
                           ?>
                           </div>
                      </div>
					    <div class="form-actions">
						  <a class="btn" href="index.php">Back</a>
					   </div>
					
					 
					</div>
				</div>
				
    </div> <!-- /container -->
  </body>
</html>